package redstar

import (
	"log"

	"gitlab.com/mergetb/xir/v0.3/go"
	. "gitlab.com/mergetb/xir/v0.3/go/build"
)

func Topo() *xir.Facility {

	tb, err := NewBuilder(
		"redstar",
		"redstar.mergetb.net",
		//XXX
		Underlay{
			Subnet:   "10.99.0.0/24",
			AsnBegin: 4200000000,
		},
		//XXX
		Underlay{
			Subnet:   "10.99.1.0/24",
			AsnBegin: 4210000000,
		},
	)
	if err != nil {
		log.Fatalf("new builder: %v", err)
	}

	proxima := tb.Nodes(128, "proxima",
		Procs(1, Cores(4)),
		Dimms(4, GB(4)),
		HDDs(1, GB(230)),
		Ipmi(1, Mbps(100)),
		Eth(1, Gbps(1), Mgmt()),
		Eth(1, Gbps(1), Infranet()),
		Eth(1, Gbps(25), Xpnet()),
		AllocModes(
			xir.AllocMode_Physical,
			xir.AllocMode_Virtual,
		),
	)

	landale := tb.Nodes(88, "landale",
		Procs(2, Cores(6)),
		Dimms(6, GB(4)),
		SSDs(1, GB(220)),
		HDDs(1, GB(960)),
		Ipmi(1, Mbps(100)),
		Eth(1, Gbps(1), Mgmt()),
		Eth(1, Gbps(1), Infranet()),
		Eth(2, Gbps(25), Xpnet()),
		AllocModes(
			xir.AllocMode_Physical,
			xir.AllocMode_Virtual,
		),
	)

	mspine := tb.MgmtSpine("mspine",
		Eth(1, Gbps(1)),
		Swp(48, Gbps(10), Mgmt()),
		Swp(6, Gbps(40), Mgmt()),
	)

	mleaf := tb.MgmtLeaves(5, "mleaf",
		Eth(1, Gbps(1)),
		Swp(48, Gbps(1), Mgmt()),
		Swp(4, Gbps(10), Mgmt()),
	)

	ispine := tb.InfraSpine("ispine",
		Eth(1, Gbps(1)),
		Swp(48, Gbps(10), Mgmt()),
		Swp(4, Gbps(40), Mgmt()),
	)

	ileaf := tb.InfraLeaves(5, "ileaf",
		Eth(1, Gbps(1)),
		Swp(48, Gbps(1), Mgmt()),
		Swp(4, Gbps(10), Mgmt()),
	)

	xleaf := tb.XpLeaves(4, "xleaf",
		Eth(1, Gbps(1), Mgmt()),
		Swp(32, Gbps(100), Xpnet()),
	)

	xspine := tb.XpSpine("xspine",
		Eth(1, Gbps(1), Mgmt()),
		Swp(32, Gbps(100), Xpnet()),
	)

	i := 0
	for _, x := range proxima {
		tb.Connect(x.Mgmt(), mleaf[i/48].NextSwp())
		tb.Connect(x.Infranet(), ileaf[i/48].NextSwp())
		i++
	}
	for _, x := range landale {
		tb.Connect(x.Mgmt(), mleaf[i/48].NextSwp())
		tb.Connect(x.Infranet(), ileaf[i/48].NextSwp())
		i++
	}
	tb.Connect(ispine.Mgmt(), mleaf[i/48].NextSwp())
	i++
	tb.Connect(xspine.Mgmt(), mleaf[i/48].NextSwp())
	i++
	for _, x := range ileaf {
		tb.Connect(x.Mgmt(), mleaf[i/48].NextSwp())
		i++
	}
	for _, x := range xleaf {
		tb.Connect(x.Mgmt(), mleaf[i/48].NextSwp())
		i++
	}

	for _, x := range mleaf {
		tb.Connect(x.NextSwpG(10), mspine.NextSwpG(10))
		tb.Connect(x.NextSwpG(10), mspine.NextSwpG(10))
		tb.Connect(x.NextSwpG(10), mspine.NextSwpG(10))
		tb.Connect(x.NextSwpG(10), mspine.NextSwpG(10))
	}

	for _, x := range ileaf {
		tb.Connect(x.NextSwpG(10), ispine.NextSwpG(10))
		tb.Connect(x.NextSwpG(10), ispine.NextSwpG(10))
		tb.Connect(x.NextSwpG(10), ispine.NextSwpG(10))
		tb.Connect(x.NextSwpG(10), ispine.NextSwpG(10))
	}

	i = 0
	for j := 0; j < len(proxima); j += 4 {
		tb.Breakout(
			xleaf[i/24].NextSwp(),
			[]*xir.Port{
				proxima[j].NextEthG(25),
				proxima[j+1].NextEthG(25),
				proxima[j+2].NextEthG(25),
				proxima[j+3].NextEthG(25),
			},
		)
		i++
	}

	for j := 0; j < len(landale); j += 2 {
		tb.Breakout(
			xleaf[i/24].NextSwp(),
			append(
				landale[j].NextEthsG(2, 25),
				landale[j+1].NextEthsG(2, 25)...,
			),
		)
		i++
	}

	for _, l := range xleaf {
		for j := 0; j < 8; j++ {
			tb.Connect(l.NextSwp(), xspine.NextSwp())
		}
	}

	return tb.Facility()

}
