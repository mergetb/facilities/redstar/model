package main

import (
	"gitlab.com/mergetb/facilities/redstar/model"
	"gitlab.com/mergetb/xir/v0.3/go/build"
)

func main() {
	build.Run(redstar.Topo())
}
