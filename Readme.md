# RedStar

### Building

```
go build
```

### Usage
```
$ ./model
Testbed build utility

Usage:
  RedStar [command]

Available Commands:
  bgp         Show BGP configuration
  cost        Calculate testbed cost
  describe    Describe a node
  help        Help about any command
  lom         Calculate testbed bill of materials
  neighbors   list a nodes neighbors
  nodes       List nodes
  power       Calculate power consumption report
  save        save the testbed model to XIR

Flags:
  -h, --help   help for RedStar

Use "RedStar [command] --help" for more information about a command.
```

![](diagram.png)
